// 导出一个数组，包含所有自定义指令，用于在main.js中导入
import dLoading from '../views/test/v-d-loading.js'

export const directives = [
    {
        'key': 'd-loading',
        'component': dLoading
    },
]
