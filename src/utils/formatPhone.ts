/**
 * 处理手机号码格式，将其中4位使用*替换
 * @param phone
 */
export function format(phone: string) {
  if (phone.length === 11) {
      return phone.replace(/(\d{3})\d{4}(\d{4})/, '$1****$2');
  } else {
  }
}