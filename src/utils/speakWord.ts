/**
 * @description 语音播报功能
 * @author lang_fuchuan
 */
export function speak(type: string) {
    const audio = new Audio(`https://dict.youdao.com/dictvoice?audio=${type}`);
    audio.play();
  }
  