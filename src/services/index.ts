import Axios, { AxiosRequestConfig } from "axios";
import { useUserStore } from "@/stores/useUser";
import router from "@/router";
import { getInfoForMain } from "@/api/getMainPageInfo";
import { showConfirmDialog, showFailToast } from "vant";

const BaseUrl = "/api";
const request = Axios.create({
  baseURL: BaseUrl,
  timeout: 5000,
});

request.interceptors.request.use(
  (config) => {
    addParamsToheader(config);
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

request.interceptors.response.use(
  (response) => {
    if (response.config.responseType === "blob") {
      return response;
    }
    const { code, msg } = response.data;
    if (code === 200) {
      return response;
    }
    rejectWithMessage(code, msg);
    return Promise.reject(msg);
  },
  (error) => {
    if (!error.response) {
      return Promise.reject("network error");
    }
    const { status, data } = error.response;
    switch (status) {
      case 400:
        return rejectWithMessage(data.code, data?.msg || "未知错误");
      case 401:
        logout();
        return rejectWithMessage(data.code, "登录已过期，请重新登录");
      case 404:
        return rejectWithMessage(data.code, "页面不存在");
      case 500:
        return rejectWithMessage(data.code, "服务错误，请联系管理员");
      case 1009:
        return getInfoForMain().then((res) => {
          useUserStore().setUserInfo(res);
        });
      default:
        return rejectWithMessage(status, data.msg);
    }
  }
);

function addParamsToheader(config: AxiosRequestConfig) {
  const token = useUserStore().token;
  if (!config.headers) {
    config.headers = {};
  }
  if (token) {
    config.headers["snow-auth"] = token;
  }
}

function rejectWithMessage(code: Number, message: string) {
  if (code === 1010) {
    return showConfirmDialog({
      title: "温馨提示",
      message: "当前非会员，免费试用次数已上限，请开通会员使用",
    })
      .then(() => {
        router.push({ name: "Space" });
      })
      .catch(() => {
        // on cancel
        router.go(-1);
      });
  }
  return showFailToast(message);
}

function logout() {
  const { logout } = useUserStore();
  logout();
  router.push("/login");
}

export default request;
