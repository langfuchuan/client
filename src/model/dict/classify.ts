export type ClassifyType = {
    id: string
    parentId: string
    code: string,
    dictKey: string,
    dictValue: string,
    remark:string
}