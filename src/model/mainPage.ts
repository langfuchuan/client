export type MainPage = {
    logo: string,
    logoText: string,
    activitiesEntity: {
        img: string,
        link: string
    },
    wordBg: string,
    vipBg: string,
    mineBg: string,
    promoteBg: string,
}

export type WordType = {
    word: string,
    pos: string,
    trans: string,
    ukphone: string,
    usphone: string,
    sentences: [
        {
            content: string,
            trans: string,
        }
    ],

}