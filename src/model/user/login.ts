export type LoginParamsType = {
    grant_type: string
    userType: number
    account:string;
    password:string;
}

export type LoginResponseType = {
    access_token: string
    token_type: string
    referesh_token: string
    userId: string
}

export type RegisterParamsType = {
    phone: string
    password: string
}

export type ResetParamsType = {
    userId: number
    oldPassword: string
    newPassword: string
}
