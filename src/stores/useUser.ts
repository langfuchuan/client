import { defineStore } from 'pinia';
import { ref } from 'vue';

export const useUserStore = defineStore('user', () => {
    const isLogin = ref(false);
    const token = ref('');
    const userInfo = ref<any>({});
    const defaultSpeak = ref('uk');

    function setToken(value: string) {
        token.value = value;
    }

    function setLoginStatus() {
        isLogin.value = true;
    }

    function setUserInfo(value: any) {
        userInfo.value = value;
    }

    function setDefaultSpeak() {
        defaultSpeak.value === 'uk' ? defaultSpeak.value = 'us' : defaultSpeak.value = 'uk';
    }

    function logout() {
        isLogin.value = false;
        token.value = '';
        userInfo.value = {};
    }

    return { isLogin, setLoginStatus, setToken, token, logout, userInfo, setUserInfo,defaultSpeak,setDefaultSpeak };
}, {
    persist: {
        enabled: true,
    },
});
