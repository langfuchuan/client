import {defineStore} from 'pinia';
import {ref, watch} from 'vue';
import router from '@/router';
import {useUserStore} from "@/stores/useUser.ts";
import {showConfirmDialog} from "vant";

export const useVipStore = defineStore('vip', () => {
    const clickNum = ref(0); // 点击次数
    // const set = new Set();

    function setClickNum(num: number) {
        clickNum.value = num;
    }
    function check() {
        if (useUserStore().token && useUserStore().userInfo.isMember === true) {
            return true;
        }
        if (clickNum.value > 5) {
            showConfirmDialog({
                title: '温馨提示',
                message:
                    '当前非会员，免费试用次数已上限，请开通会员使用',
            })
                .then(() => {
                    router.push({ name: 'Space' });
                })
                .catch(() => {
                    // on cancel
                    router.go(-1)
                });
            return false;
        }
        return true;
    }
    function getClickNum() {
        return clickNum.value;
    }

    watch(clickNum, (newVal) => {
        check();
    });

    return {
        clickNum,
        check,
        setClickNum,
        getClickNum
    };
});