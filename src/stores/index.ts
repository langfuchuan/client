import {createPinia} from 'pinia';

import  createPersistPlugin  from 'pinia-plugin-persist'
const pinia = createPinia();
pinia.use(createPersistPlugin);
export default pinia;