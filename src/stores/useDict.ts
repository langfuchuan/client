import {defineStore} from "pinia";
import {ref} from "vue";
import {ClassifyType} from "@/model/dict/classify.ts";

export const useDictStore = defineStore("dict", () => {
    const classifyDict = ref<ClassifyType[]>([])
    // const bookDict = ref([])
    function setDictList(data:ClassifyType[]) {
        classifyDict.value = data
    }
    return {classifyDict,setDictList}
})