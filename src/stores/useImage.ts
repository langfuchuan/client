import {defineStore} from 'pinia'
import {ref} from 'vue'


export const useImageStore = defineStore('image', () => {
    const wordBg = ref('')
    const vipBg = ref('')
    const promotionsBg = ref('')
    const mineBg = ref('')
    function setWordBg(url: string) { 
        wordBg.value = url
    }
    function getWordBg() {
        return wordBg.value
    }
    function setVipBg(url: string) {
        vipBg.value = url
    }
    function getVipBg() {
        return vipBg.value
    }
    function setPromotionsBg(url: string) {
        promotionsBg.value = url
    }
    function getPromotionsBg() {
        return promotionsBg.value
    }
    function setMineBg(url: string) {
        mineBg.value = url
    }
    function getMineBg() {
        return mineBg.value
    }
    return {
        wordBg,vipBg,promotionsBg,mineBg,
        setWordBg,getWordBg,
        setVipBg,getVipBg,
        setPromotionsBg,getPromotionsBg,
        setMineBg,getMineBg
    }
},{
    persist: {
        enabled: true,
    },
})