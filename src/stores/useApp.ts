import {defineStore} from 'pinia';
import piniaStore from '@/stores/index';
// @ts-ignore
export const useAppStore = defineStore('app',
    // 唯一ID
    {
        state: () => ({
            transitionName: '',
        }),
        getters: {
            getTransitionName(state) {
                return state.transitionName;
            },
        },
        actions: {
            // 更改过渡动画
            setTransitionName(value: string) {
                this.transitionName = value;
            },
            // 处理路由跳转
            handleRouter(to: any, from: any) {
                if (to.meta.index > from.meta.index) {
                    // 向左滑动
                    this.setTransitionName('slide-left');
                } else if (to.meta.index < from.meta.index) {
                    // 由次级到主级
                    this.setTransitionName('slide-right');
                } else {
                    this.setTransitionName('');
                }
            },
        }
    }
);

export function useAppOutsideStore() {
    return useAppStore(piniaStore);
}
