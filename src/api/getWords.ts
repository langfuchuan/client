import request from "@/services";
import {RouteParamValue} from "vue-router";


type RequestParams = {
    id?: string
    type?: string
}


async function getTextbookList(params:RequestParams){
    return request.get('/word-classify/searchTypeClassify',{params}).then(res=> res.data.data)
}

async function getClassifyList(params: { id: string | RouteParamValue[] }){
    return request.get('/word-classify/tree',{params}).then(res=> res.data.data)
}

async function getWordDetail(id:string){
    return request.get(`/word-classify/${id}`).then(res=> res.data.data)
}

async function getWordList(params: { size: number; classifyType: string | RouteParamValue[] }) {
    return request.get('/app/word/pageList',{params}).then(res => res.data.data)
}

function  getWordInfo(id:string) {
    return request.get(`/app/word/${id}`).then(res => res.data.data)
}

async function getWordForSearch(params:{word:string}) {
    return request.get('/app/word/pageList',{params:params}).then(res => res.data.data)
}

function addCollectWord(body: { wordId: any }) {
     return request.post('/user/word/collect',body).then(res => res.data.data)
}

function cancelCollectWord(body: { wordId: any}) {
    return request.post('/user/word/cancelCollect',body).then(res => res.data.data)
}

function getCollectWordList() {
    return request.get('/user/collect/pageList').then(res => res.data.data.records)
}

function addErrorWord(wordId: string) {
     return request.post(`/user/wrongBook/add2WrongBook/${wordId}`).then(res => res.data.data)
}

function cancelErrorWord(ids: string) {
    return request.post(`/user/wrongBook/remove/${ids}`).then(res => res.data.data)
}

function getErrorWordList() {
    return request.get('/user/wrongBook/pageList').then(res => res.data.data.records)
}


export {getTextbookList,getClassifyList,getWordDetail,getWordList,getWordInfo,getWordForSearch,addCollectWord,cancelCollectWord,getCollectWordList,addErrorWord,cancelErrorWord,getErrorWordList}