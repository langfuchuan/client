import request from "@/services";
import {MainPage, WordType} from "@/model/mainPage.ts";

async function getInfoForMain() {
    return request.get('app/config/get').then(res => res.data.data as MainPage)
}

async function  getRandomWord() {
    return request.get('app/word/randomList').then(res => res.data.data as WordType[])
}

export {getInfoForMain, getRandomWord}