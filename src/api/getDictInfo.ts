import request from "@/services";
import {ClassifyType} from "@/model/dict/classify.ts";


type params = {
    parentId:string
}
async function getTabs(params:params) {
    return request.get('/sys-dict/list', {params}).then(res => res.data.data as ClassifyType[])
}

export {getTabs}