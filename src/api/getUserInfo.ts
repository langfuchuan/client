import request from "@/services/index";
import {LoginParamsType, LoginResponseType, RegisterParamsType} from '@/model/user/login';
import {userType} from "@/model/user/user";

const headers = {
    Authorization: "Basic d2ViOndlYl9zZWNyZXQ=",
};

async function login(params: LoginParamsType) {
    const {grant_type, userType, account, password} = params
    return request.post(`/oauth/token?grant_type=${grant_type}&userType=${userType}&account=${account}&password=${password}`, {}, {
        headers: headers
    }).then(res => res.data.data as LoginResponseType)
}

async function bindPhone(phone: string) {
    return request.post(`/common/bindPhone?phone=${phone}`, {}, {
        headers: headers
    }).then(res => res.data as LoginResponseType)
}

async function wx_login(params: any) {
    const {grant_type, code, userType} = params
    return request.post(`/oauth/token?grant_type=${grant_type}&userType=${userType}&code=${code}`, {}, {
        headers: headers
    }).then(res => res.data.data as LoginResponseType)
}

async function getOpenIdInfo(code: string) {
    return request.get(`/common/getOpenId?code=${code}`, {
        headers: headers
    }).then(res => res.data.data as LoginResponseType)
}

async function register(data: RegisterParamsType) {
    return request.post('/user/register', data).then(res => res.data)

}

async function reset(data: any) {
    return request.post('/sys-user/resetPwd', data).then(res => res.data)
}

async function getUserInfo(id: number) {
    return request.get(`/sys-user/${id}`).then(res => res.data.data as userType[])
}

async function getCurrentUserInfo() {
    return request.get('/user/info').then(res => res.data.data)
}

async function getQrCode() {
    return request.get('/user/qrcode', {responseType: "blob"}).then(res => res.data)
}

async function getCollection() {
    return request.get('/user/pageList').then(res => res.data.data)
}

export {login, getUserInfo, reset, register, getQrCode, getCollection, getCurrentUserInfo, wx_login, getOpenIdInfo,bindPhone}
