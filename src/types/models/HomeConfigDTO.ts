/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type HomeConfigDTO = {
    bgColor?: string;
    color?: string;
    duration?: number;
    name?: string;
    percent?: string;
    value?: number;
};
