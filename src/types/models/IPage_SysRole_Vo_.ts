/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SysRole_Vo } from './SysRole_Vo';

export type IPage_SysRole_Vo_ = {
    current?: number;
    pages?: number;
    records?: Array<SysRole_Vo>;
    size?: number;
    total?: number;
};
