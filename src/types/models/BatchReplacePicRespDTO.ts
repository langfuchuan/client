/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type BatchReplacePicRespDTO = {
    errorList?: Array<string>;
    successList?: Array<string>;
};
