/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 会员卡表
 */
export type MemberCard_Vo0 = {
    /**
     * 批量生成数量
     */
    batchCount?: number;
    /**
     * 会员卡号
     */
    cardNumber?: string;
    /**
     * 创建时间
     */
    createTime?: string;
    /**
     * 创建者ID
     */
    createUserId?: number;
    /**
     * 创建者名字
     */
    createUserName?: string;
    /**
     * 主键id
     */
    id?: number;
    /**
     * 删除标记
     */
    isDeleted?: number;
    /**
     * 备注
     */
    remark?: string;
    /**
     * 默认状态
     */
    status?: number;
    /**
     * 会员卡类型
     */
    type?: string;
    /**
     * 更新时间
     */
    updateTime?: string;
    /**
     * 更新者ID
     */
    updateUserId?: number;
    /**
     * 更新者名字
     */
    updateUserName?: string;
    /**
     * 使用该卡号的用户
     */
    userId?: number;
    /**
     * 用户类型
     */
    userType?: number;
};
