/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AttachDTO } from './AttachDTO';

/**
 * 返回信息
 */
export type R_AttachDTO_ = {
    /**
     * 状态码
     */
    code: number;
    /**
     * 承载数据
     */
    data?: AttachDTO;
    /**
     * 返回消息
     */
    msg: string;
    /**
     * 是否成功
     */
    success: boolean;
};
