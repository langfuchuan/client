/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 字典表
 */
export type SysDict_Vo0 = {
    /**
     * 字典码
     */
    code?: string;
    /**
     * 创建时间
     */
    createTime?: string;
    /**
     * 创建者ID
     */
    createUserId?: number;
    /**
     * 创建者名字
     */
    createUserName?: string;
    /**
     * 字典值
     */
    dictKey?: string;
    /**
     * 字典名称
     */
    dictValue?: string;
    /**
     * 主键id
     */
    id?: number;
    /**
     * 删除标记
     */
    isDeleted?: number;
    /**
     * 是否已封存
     */
    isSealed?: number;
    /**
     * 父主键
     */
    parentId?: number;
    /**
     * 字典备注
     */
    remark?: string;
    /**
     * 排序
     */
    sort?: number;
    /**
     * 默认状态
     */
    status?: number;
    /**
     * 更新时间
     */
    updateTime?: string;
    /**
     * 更新者ID
     */
    updateUserId?: number;
    /**
     * 更新者名字
     */
    updateUserName?: string;
};
