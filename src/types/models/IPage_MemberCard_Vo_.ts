/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { MemberCard_Vo } from './MemberCard_Vo';

export type IPage_MemberCard_Vo_ = {
    current?: number;
    pages?: number;
    records?: Array<MemberCard_Vo>;
    size?: number;
    total?: number;
};
