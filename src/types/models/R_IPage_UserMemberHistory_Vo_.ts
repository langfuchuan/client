/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IPage_UserMemberHistory_Vo_ } from './IPage_UserMemberHistory_Vo_';

/**
 * 返回信息
 */
export type R_IPage_UserMemberHistory_Vo_ = {
    /**
     * 状态码
     */
    code: number;
    /**
     * 承载数据
     */
    data?: IPage_UserMemberHistory_Vo_;
    /**
     * 返回消息
     */
    msg: string;
    /**
     * 是否成功
     */
    success: boolean;
};
