/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 会员用户表
 */
export type UserMember_Vo = {
    /**
     * 会员卡号
     */
    cardNumber?: string;
    /**
     * 创建时间
     */
    createTime?: string;
    /**
     * 创建者ID
     */
    createUserId?: number;
    /**
     * 创建者名字
     */
    createUserName?: string;
    /**
     * 到期时间
     */
    expireTime?: string;
    /**
     * 主键id
     */
    id?: number;
    /**
     * 删除标记
     */
    isDeleted?: number;
    /**
     * 手机号
     */
    phone?: string;
    /**
     * 用户姓名
     */
    realName?: string;
    /**
     * 默认状态
     */
    status?: number;
    /**
     * 更新时间
     */
    updateTime?: string;
    /**
     * 更新者ID
     */
    updateUserId?: number;
    /**
     * 更新者名字
     */
    updateUserName?: string;
    /**
     * 用户id
     */
    userId?: number;
};
