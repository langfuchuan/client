/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 单词表
 */
export type SysWordInfo_Vo0 = {
    /**
     * 音频链接
     */
    audio?: string;
    /**
     * 创建时间
     */
    createTime?: string;
    /**
     * 创建者ID
     */
    createUserId?: number;
    /**
     * 创建者名字
     */
    createUserName?: string;
    /**
     * 文件标识id
     */
    fileId?: string;
    /**
     * 首字母
     */
    firstLetter?: string;
    /**
     * 主键id
     */
    id?: number;
    /**
     * 学习图片链接
     */
    img?: string;
    /**
     * 删除标记
     */
    isDeleted?: number;
    /**
     * 默认状态
     */
    status?: number;
    /**
     * 更新时间
     */
    updateTime?: string;
    /**
     * 更新者ID
     */
    updateUserId?: number;
    /**
     * 更新者名字
     */
    updateUserName?: string;
    /**
     * 单词
     */
    word?: string;
};
