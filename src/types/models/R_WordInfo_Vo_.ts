/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { WordInfo_Vo } from './WordInfo_Vo';

/**
 * 返回信息
 */
export type R_WordInfo_Vo_ = {
    /**
     * 状态码
     */
    code: number;
    /**
     * 承载数据
     */
    data?: WordInfo_Vo;
    /**
     * 返回消息
     */
    msg: string;
    /**
     * 是否成功
     */
    success: boolean;
};
