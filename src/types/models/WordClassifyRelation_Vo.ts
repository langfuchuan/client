/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 单词分类中间表
 */
export type WordClassifyRelation_Vo = {
    /**
     * 祖籍列表
     */
    ancestors?: string;
    /**
     * 分类id
     */
    classifyId?: number;
    /**
     * 主键id
     */
    id?: number;
    /**
     * 单词id
     */
    wordId?: number;
    /**
     * 单词ids
     */
    wordIds?: Array<number>;
};
