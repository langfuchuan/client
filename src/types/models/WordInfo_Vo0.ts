/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { WordPhrases_ } from './WordPhrases_';
import type { WordRelword_ } from './WordRelword_';
import type { WordSentences_ } from './WordSentences_';
import type { WordSynos_ } from './WordSynos_';

/**
 * 单词表
 */
export type WordInfo_Vo0 = {
    /**
     * 查询单词分类
     */
    classifyType?: string;
    /**
     * 例句
     */
    content?: string;
    /**
     * 创建时间
     */
    createTime?: string;
    /**
     * 创建者ID
     */
    createUserId?: number;
    /**
     * 创建者名字
     */
    createUserName?: string;
    /**
     * 排除查询单词分类
     */
    excludeClassifyTypeWord?: string;
    /**
     * 首字母
     */
    firstLetter?: string;
    /**
     * 主键id
     */
    id?: number;
    /**
     * 学习图片链接
     */
    img?: string;
    /**
     * 删除标记
     */
    isDeleted?: number;
    /**
     * 短语
     */
    phrases?: Array<WordPhrases_>;
    /**
     * 词性
     */
    pos?: string;
    /**
     * 关联词
     */
    relwords?: Array<WordRelword_>;
    /**
     * 单词记忆
     */
    remMethod?: string;
    /**
     * 例句
     */
    sentences?: Array<WordSentences_>;
    /**
     * 排序
     */
    sort?: number;
    /**
     * 默认状态
     */
    status?: number;
    /**
     * 同义词
     */
    synos?: Array<WordSynos_>;
    /**
     * 中文翻译
     */
    trans?: string;
    /**
     * 英音音标
     */
    ukphone?: string;
    /**
     * 英音发音https请求参数
     */
    ukspeech?: string;
    /**
     * 更新时间
     */
    updateTime?: string;
    /**
     * 更新者ID
     */
    updateUserId?: number;
    /**
     * 更新者名字
     */
    updateUserName?: string;
    /**
     * 美音音标
     */
    usphone?: string;
    /**
     * 美音发音https请求参数
     */
    usspeech?: string;
    /**
     * 单词
     */
    word?: string;
};
