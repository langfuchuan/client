/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SysDict_Vo0 } from './SysDict_Vo0';

export type IPage_SysDict_Vo_ = {
    current?: number;
    pages?: number;
    records?: Array<SysDict_Vo0>;
    size?: number;
    total?: number;
};
