/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SysWordInfo_Vo } from './SysWordInfo_Vo';

export type IPage_SysWordInfo_Vo_ = {
    current?: number;
    pages?: number;
    records?: Array<SysWordInfo_Vo>;
    size?: number;
    total?: number;
};
