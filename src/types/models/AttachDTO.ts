/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AttachDTO = {
    /**
     * 附件大小
     */
    attachSize?: number;
    /**
     * 附件域名
     */
    domainUrl?: string;
    /**
     * 附件拓展名
     */
    extension?: string;
    /**
     * id
     */
    id?: number;
    /**
     * 附件地址
     */
    link?: string;
    /**
     * 文件md5
     */
    md5?: string;
    /**
     * 附件名称
     */
    name?: string;
    /**
     * 附件原名
     */
    originalName?: string;
};
