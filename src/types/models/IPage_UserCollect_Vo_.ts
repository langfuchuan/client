/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { UserCollect_Vo } from './UserCollect_Vo';

export type IPage_UserCollect_Vo_ = {
    current?: number;
    pages?: number;
    records?: Array<UserCollect_Vo>;
    size?: number;
    total?: number;
};
