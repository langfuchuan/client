/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Meta = {
    auths?: Array<string>;
    icon?: string;
    keepAlive?: boolean;
    roles?: Array<string>;
    showLink?: boolean;
    showParent?: boolean;
    title?: string;
};
