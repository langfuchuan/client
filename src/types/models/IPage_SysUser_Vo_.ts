/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SysUser_Vo0 } from './SysUser_Vo0';

export type IPage_SysUser_Vo_ = {
    current?: number;
    pages?: number;
    records?: Array<SysUser_Vo0>;
    size?: number;
    total?: number;
};
