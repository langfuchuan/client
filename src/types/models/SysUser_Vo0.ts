/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 用户表
 */
export type SysUser_Vo0 = {
    /**
     * 账号
     */
    account?: string;
    /**
     * 头像
     */
    avatar?: string;
    /**
     * 生日
     */
    birthday?: string;
    /**
     * 创建时间
     */
    createTime?: string;
    /**
     * 创建者ID
     */
    createUserId?: number;
    /**
     * 创建者名字
     */
    createUserName?: string;
    /**
     * 邮箱
     */
    email?: string;
    /**
     * 主键id
     */
    id?: number;
    /**
     * 删除标记
     */
    isDeleted?: number;
    /**
     * 最后登录时间
     */
    loginDate?: string;
    /**
     * 最后登录IP
     */
    loginIp?: string;
    /**
     * 如果是会员，会员到期时间
     */
    memberExpireTime?: string;
    /**
     * 是否是会员
     */
    memberStatus?: boolean;
    /**
     * 昵称
     */
    nickName?: string;
    /**
     * 密码
     */
    password?: string;
    /**
     * 手机
     */
    phone?: string;
    /**
     * 用户名
     */
    realName?: string;
    /**
     * 备注
     */
    remark?: string;
    /**
     * 角色id
     */
    roleId?: string;
    /**
     * 角色标识集合
     */
    roles?: Array<string>;
    /**
     * 性别
     */
    sex?: number;
    /**
     * 默认状态
     */
    status?: number;
    /**
     * 更新时间
     */
    updateTime?: string;
    /**
     * 更新者ID
     */
    updateUserId?: number;
    /**
     * 更新者名字
     */
    updateUserName?: string;
    /**
     * 用户类型
     */
    userType?: number;
};
