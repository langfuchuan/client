/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { UserMemberHistory_Vo } from './UserMemberHistory_Vo';

export type IPage_UserMemberHistory_Vo_ = {
    current?: number;
    pages?: number;
    records?: Array<UserMemberHistory_Vo>;
    size?: number;
    total?: number;
};
