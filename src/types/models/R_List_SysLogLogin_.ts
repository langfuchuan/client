/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SysLogLogin_ } from './SysLogLogin_';

/**
 * 返回信息
 */
export type R_List_SysLogLogin_ = {
    /**
     * 状态码
     */
    code: number;
    /**
     * 承载数据
     */
    data?: Array<SysLogLogin_>;
    /**
     * 返回消息
     */
    msg: string;
    /**
     * 是否成功
     */
    success: boolean;
};
