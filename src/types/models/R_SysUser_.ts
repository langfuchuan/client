/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SysUser_ } from './SysUser_';

/**
 * 返回信息
 */
export type R_SysUser_ = {
    /**
     * 状态码
     */
    code: number;
    /**
     * 承载数据
     */
    data?: SysUser_;
    /**
     * 返回消息
     */
    msg: string;
    /**
     * 是否成功
     */
    success: boolean;
};
