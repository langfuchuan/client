/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SysAttach_Vo } from './SysAttach_Vo';

export type IPage_SysAttach_Vo_ = {
    current?: number;
    pages?: number;
    records?: Array<SysAttach_Vo>;
    size?: number;
    total?: number;
};
