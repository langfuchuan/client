/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * app首页配置
 */
export type HomeConfig_Vo0 = {
    /**
     * 活动 json格式 有img 和link属性
     */
    activities?: string;
    /**
     * 活动实体
     */
    activitiesEntity?: Array<any>;
    /**
     * 创建时间
     */
    createTime?: string;
    /**
     * 创建者ID
     */
    createUserId?: number;
    /**
     * 创建者名字
     */
    createUserName?: string;
    /**
     * 主键id
     */
    id?: number;
    /**
     * 删除标记
     */
    isDeleted?: number;
    /**
     * logo
     */
    logo?: string;
    /**
     * logo后接的文字
     */
    logoText?: string;
    /**
     * 默认状态
     */
    status?: number;
    /**
     * 更新时间
     */
    updateTime?: string;
    /**
     * 更新者ID
     */
    updateUserId?: number;
    /**
     * 更新者名字
     */
    updateUserName?: string;
    /**
     * 首页欢迎图片，多张逗号分隔
     */
    welImgs?: string;
};
