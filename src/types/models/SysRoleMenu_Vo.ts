/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 角色菜单关联表
 */
export type SysRoleMenu_Vo = {
    /**
     * 主键id
     */
    id?: number;
    /**
     * 菜单id
     */
    menuId?: number;
    /**
     * 菜单id集合
     */
    menuIds?: string;
    /**
     * 角色id
     */
    roleId?: number;
};
