/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { UserMember_Vo } from './UserMember_Vo';

export type IPage_UserMember_Vo_ = {
    current?: number;
    pages?: number;
    records?: Array<UserMember_Vo>;
    size?: number;
    total?: number;
};
