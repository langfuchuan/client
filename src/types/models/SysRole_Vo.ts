/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 角色表
 */
export type SysRole_Vo = {
    /**
     * 创建时间
     */
    createTime?: string;
    /**
     * 创建者ID
     */
    createUserId?: number;
    /**
     * 创建者名字
     */
    createUserName?: string;
    /**
     * 主键id
     */
    id?: number;
    /**
     * 删除标记
     */
    isDeleted?: number;
    /**
     * 父主键
     */
    parentId?: number;
    /**
     * 备注
     */
    remark?: string;
    /**
     * 角色别名
     */
    roleAlias?: string;
    /**
     * 角色名
     */
    roleName?: string;
    /**
     * 排序
     */
    sort?: number;
    /**
     * 默认状态
     */
    status?: number;
    /**
     * 更新时间
     */
    updateTime?: string;
    /**
     * 更新者ID
     */
    updateUserId?: number;
    /**
     * 更新者名字
     */
    updateUserName?: string;
};
