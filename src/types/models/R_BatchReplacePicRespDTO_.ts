/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { BatchReplacePicRespDTO } from './BatchReplacePicRespDTO';

/**
 * 返回信息
 */
export type R_BatchReplacePicRespDTO_ = {
    /**
     * 状态码
     */
    code: number;
    /**
     * 承载数据
     */
    data?: BatchReplacePicRespDTO;
    /**
     * 返回消息
     */
    msg: string;
    /**
     * 是否成功
     */
    success: boolean;
};
