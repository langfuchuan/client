/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ChangePwdDTO = {
    newPassword?: string;
    oldPassword?: string;
    userId?: number;
};
