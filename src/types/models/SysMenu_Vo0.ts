/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Meta } from './Meta';

/**
 * 菜单表
 */
export type SysMenu_Vo0 = {
    /**
     * 菜单激活（将某个菜单激活，主要用于通过query或params传参的路由，当它们通过配置showLink: false后不在菜单中显示，就不会有任何菜单高亮，而通过设置activePath指定激活菜单即可获得高亮，activePath为指定激活菜单的path）
     */
    activePath?: string;
    /**
     * 权限标识（按钮级别权限设置）
     */
    auths?: string;
    /**
     * 是否选中
     */
    checked?: boolean;
    children?: Array<SysMenu_Vo0>;
    /**
     * 进场动画（页面加载动画）
     */
    enterTransition?: string;
    /**
     * 右侧图标
     */
    extraIcon?: string;
    /**
     * 加载动画（内嵌的iframe页面是否开启首次加载动画）
     */
    frameLoading?: string;
    /**
     * 链接地址（需要内嵌的iframe链接地址）
     */
    frameSrc?: string;
    hasChildren?: boolean;
    /**
     * 标签页（当前菜单名称或自定义信息禁止添加到标签页）
     */
    hiddenTag?: boolean;
    /**
     * 菜单icon
     */
    icon?: string;
    /**
     * 主键id
     */
    id?: number;
    /**
     * 删除标记
     */
    isDeleted?: number;
    /**
     * 缓存页面（是否缓存该路由页面，开启后会保存该页面的整体状态，刷新后会清空状态）
     */
    keepAlive?: boolean;
    /**
     * 离场动画（页面加载动画）
     */
    leaveTransition?: string;
    /**
     * 菜单类型（0代表菜单、1代表iframe、2代表外链、3代表按钮）
     */
    menuType?: number;
    meta?: Meta;
    /**
     * 组件名称
     */
    name?: string;
    /**
     * 父级菜单
     */
    parentId?: number;
    /**
     * 请求地址
     */
    path?: string;
    /**
     * 路由重定向
     */
    redirect?: string;
    /**
     * 备注
     */
    remark?: string;
    /**
     * 菜单（是否显示该菜单）
     */
    showLink?: boolean;
    /**
     * 父级菜单（是否显示父级菜单 
     */
    showParent?: boolean;
    /**
     * 排序
     */
    sort?: number;
    /**
     * 菜单名称
     */
    title?: string;
};
