/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SysUser_Vo0 } from './SysUser_Vo0';

/**
 * 返回信息
 */
export type R_SysUser_Vo_ = {
    /**
     * 状态码
     */
    code: number;
    /**
     * 承载数据
     */
    data?: SysUser_Vo0;
    /**
     * 返回消息
     */
    msg: string;
    /**
     * 是否成功
     */
    success: boolean;
};
