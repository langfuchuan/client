/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 附件表
 */
export type SysAttach_Vo = {
    /**
     * 附件大小
     */
    attachSize?: number;
    /**
     * 创建时间
     */
    createTime?: string;
    /**
     * 创建人
     */
    createUser?: number;
    /**
     * 创建者ID
     */
    createUserId?: number;
    /**
     * 创建者名字
     */
    createUserName?: string;
    /**
     * 附件域名
     */
    domainUrl?: string;
    /**
     * 附件拓展名
     */
    extension?: string;
    /**
     * 主键id
     */
    id?: number;
    /**
     * 删除标记
     */
    isDeleted?: number;
    /**
     * 附件地址
     */
    link?: string;
    /**
     * 文件md5
     */
    md5?: string;
    /**
     * 附件名称
     */
    name?: string;
    /**
     * 附件原名
     */
    originalName?: string;
    /**
     * 默认状态
     */
    status?: number;
    /**
     * 更新时间
     */
    updateTime?: string;
    /**
     * 修改人
     */
    updateUser?: number;
    /**
     * 更新者ID
     */
    updateUserId?: number;
    /**
     * 更新者名字
     */
    updateUserName?: string;
};
