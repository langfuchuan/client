/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 单词分类表
 */
export type WordClassify_Vo = {
    /**
     * 创建时间
     */
    createTime?: string;
    /**
     * 创建者ID
     */
    createUserId?: number;
    /**
     * 创建者名字
     */
    createUserName?: string;
    /**
     * 分类描述
     */
    description?: string;
    /**
     * 主键id
     */
    id?: number;
    /**
     * 删除标记
     */
    isDeleted?: number;
    /**
     * 分类名称
     */
    name?: string;
    /**
     * 父级分类
     */
    parentId?: number;
    /**
     * 排序
     */
    sort?: number;
    /**
     * 默认状态
     */
    status?: number;
    /**
     * 所属板块
     */
    type?: string;
    /**
     * 更新时间
     */
    updateTime?: string;
    /**
     * 更新者ID
     */
    updateUserId?: number;
    /**
     * 更新者名字
     */
    updateUserName?: string;
};
