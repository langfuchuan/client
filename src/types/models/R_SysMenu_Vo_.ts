/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SysMenu_Vo0 } from './SysMenu_Vo0';

/**
 * 返回信息
 */
export type R_SysMenu_Vo_ = {
    /**
     * 状态码
     */
    code: number;
    /**
     * 承载数据
     */
    data?: SysMenu_Vo0;
    /**
     * 返回消息
     */
    msg: string;
    /**
     * 是否成功
     */
    success: boolean;
};
