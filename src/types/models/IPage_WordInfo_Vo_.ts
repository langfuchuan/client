/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { WordInfo_Vo } from './WordInfo_Vo';

export type IPage_WordInfo_Vo_ = {
    current?: number;
    pages?: number;
    records?: Array<WordInfo_Vo>;
    size?: number;
    total?: number;
};
