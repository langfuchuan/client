/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * 同根词
 */
export type WordRelword_ = {
    /**
     * 例句
     */
    content?: string;
    /**
     * 创建时间
     */
    createTime?: string;
    /**
     * 创建者ID
     */
    createUserId?: number;
    /**
     * 创建者名字
     */
    createUserName?: string;
    /**
     * 主键id
     */
    id?: number;
    /**
     * 删除标记
     */
    isDeleted?: number;
    /**
     * 词性
     */
    pos?: string;
    /**
     * 默认状态
     */
    status?: number;
    /**
     * 中文翻译
     */
    trans?: string;
    /**
     * 更新时间
     */
    updateTime?: string;
    /**
     * 更新者ID
     */
    updateUserId?: number;
    /**
     * 更新者名字
     */
    updateUserName?: string;
    /**
     * 单词
     */
    word?: string;
    /**
     * 单表id
     */
    wordId?: number;
};
