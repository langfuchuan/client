/**
 * 全局loading指令
 */
// 导入loading组件
// import LoadingComponent1 from '@/components/data-loading/index.vue'
import LoadingComponent2 from '@/views/test/index2.vue'
import LoadingComponent3 from '@/views/test/index3.vue'
import {createApp, ref} from 'vue'

const loadingMap = new WeakMap()
const LoadingComponent = ref(null)

function unmounted(el) {
    if (loadingMap.has(el)) {
        loadingMap.get(el).$el.remove()
        loadingMap.delete(el)
    }
}

/**
 * 判断参数类型
 * @param binding
 */
const resolveParam = (binding) => {
    let loadingFlag

    function _getComponent(type) {
        switch (type) {
            // case 1:
            //     return LoadingComponent1
            case 2:
                return LoadingComponent2
            case 3:
                return LoadingComponent3
            default:
                throw new Error('未找到对应的loading组件')
        }
    }

    // 如果是对象
    if (binding.value && typeof binding.value === 'object') {
        loadingFlag = binding.value.loading
    } else {
        loadingFlag = binding.value ?? true
    }
    LoadingComponent.value = _getComponent(binding.value?.type ?? 2)
    return loadingFlag
}

function mounted(el, binding) {
    // 判断是否需要loading
    const loading = resolveParam(binding)
    if (loading === false) {
        return;
    }
    if (loadingMap.has(el)) {
        return
    }
    // 创建组件实例
    const app = createApp(LoadingComponent.value, {})
    // 挂载
    const loadingInstance = app.mount(document.createElement('div'))
    // 将loading实例挂载到dom上
    el.appendChild(loadingInstance.$el)
    // 将loading实例绑定到dom上
    el.__loadingInstance__ = loadingInstance
    loadingMap.set(el, loadingInstance)
}

export default {
    beforeUpdate(el, binding) {
        const loading = resolveParam(binding)
        if (loading === false) {
            unmounted(el)
            return
        }
        mounted(el, binding)
    },
    mounted(el, binding) {
        mounted(el, binding)
    },
    unmounted(el) {
        unmounted(el)
    }
}
