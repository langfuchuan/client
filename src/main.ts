import { createApp } from 'vue'
import App from './App.vue'
import router from '@/router/index';
import pinia from '@/stores/index';
import 'uno.css';
import '@unocss/reset/tailwind.css'
import '@/style/index.scss';
import 'element-plus/dist/index.css'
import "swiper/css";
import 'vant/lib/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
// @ts-ignore
import {directives} from '@/utils/cs-directive.js'
Object.keys(directives).forEach(key => {
  const com = directives[key]['component']
  app.directive(directives[key]['key'], {
    beforeUpdate(el, binding) {
      com.beforeUpdate &&
      com.beforeUpdate(el, binding)
    },
    mounted(el, binding) {
      com.mounted(el, binding)
    },
    unmounted(el, binding) {
      com.unmounted(el, binding)
    },
  })
})
app.use(pinia)
pinia.use(piniaPluginPersistedstate);
app.use(router)
app.mount('#app')
