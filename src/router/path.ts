import {RouteRecordRaw} from "vue-router";

const routes: RouteRecordRaw[] = [
    {
        path: '/',
        redirect: '/home',
    },
    {
        path: '/home',
        name: 'Home',
        meta: {
            index: 1,
            title: '首页',
        },
        component: () => import('@/views/Home.vue'),
    },
    {
        path: '/login',
        name: 'Login',
        meta: {
            index: 2,
            title: '登录',
        },
        component: () => import('@/views/login/Login.vue'),
    },
    {
        path: '/bindPhone',
        name: 'BindPhone',
        meta: {
            index: 2,
            title: '绑定手机号',
        },
        component: () => import('@/views/login/bindPhone.vue'),
    },
    {
        path: '/register',
        name: 'Register',
        meta: {
            index: 3,
            title: '注册',
        },
        component: () => import('@/views/login/Register.vue'),
    },
    {
        path: '/change',
        name: 'Change',
        meta: {
            index: 3,
            title: '修改密码',
        },
        component: () => import('@/views/login/ChangePassword.vue'),
    },
    {
        path: '/space',
        name: 'Space',
        meta: {
            index: 3,
            title: '空间',
        },
        component: () => import('@/views/Space.vue')
    },
    {
        path: "/personal",
        name: "Personal",
        meta: {
            index: 2,
            title: '个人中心',
        },
        component: () => import("@/views/Personal.vue"),
    },
    {
        path: '/textbook/:type',
        name: 'Textbook',
        meta: {
            index: 2,
            title: '课本',
        },
        component: () => import('@/views/TextBook.vue'),
    },
    {
        path: '/classify/:id',
        name: 'Classify',
        meta: {
            index: 3,
            title: '分类',
        },
        component: () => import('@/views/Classify.vue'),
        props: true
    },
    {
        path: '/unit/:id',
        name: 'Unit',
        meta: {
            index: 3,
            title: '单元列表',
        },
        component: () => import('@/views/Unit.vue'),
        props: true
    },
    {
        path: '/word/:id',
        name: 'Word',
        meta: {
            index: 4,
            title: '单词列表',
        },
        component: () => import('@/views/Words.vue'),
        props: true
    },
    {
        path: '/learn',
        name: 'Learn',
        meta: {
            index: 5,
            title: '单词详情',
        },
        component: () => import('@/views/Learn.vue'),
        props: true
    },
    {
        path: '/dictation/:id',
        name: 'Dictation',
        meta: {
            index: 5,
            title: '单词默写',
        },
        component: () => import('@/views/Dictation.vue'),
        props: true
    },
    {
        path: '/edit',
        name: 'Edit',
        meta: {
            index: 3,
            title: '编辑资料',
        },
        component: () => import('@/views/UserEdit.vue'),
    },
    {
        path: '/collect',
        name: 'Collect',
        meta: {
            index: 3,
            title: '收藏',
        },
        component: () => import('@/views/Collect.vue')
    },
    {
        path: '/notebook',
        name: 'Notebook',
        meta: {
            index: 3,
            title: '错题本',
        },
        component: () => import('@/views/Notebook.vue')
    },
    {
        path: '/qrCode',
        name: 'QrCode',
        meta: {
            index: 3,
            title: '二维码',
        },
        component: () => import('@/views/QrCode.vue')
    },
    {
        path: '/setting',
        name: 'Setting',
        meta: {
            index: 3,
            title: '设置',
        },
        component: () => import('@/views/Setting.vue')
    },
    {
        path: '/search',
        name: 'Search',
        meta: {
            index: 2,
            title: '搜索',
        },
        component: () => import('@/views/Search.vue')
    },
    {
        path: '/test',
        name: 'test',
        component: () => import('@/views/Test.vue'),
        props: route => ({ arr: route.params.arr }),
    }
]

export default routes