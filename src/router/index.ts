import {
  createRouter,
  createWebHistory,
  NavigationGuardNext,
  RouteRecordRaw,
} from "vue-router";
import paths from "@/router/path";
import { useAppOutsideStore } from "@/stores/useApp";
import { useUserStore } from "@/stores/useUser.ts";
import { useVipStore } from "@/stores/useVip";

const appStore = useAppOutsideStore();

const routes: RouteRecordRaw[] = paths;
const scrollBehavior = (to: any, _from: any, savedPosition: any) => {
  if (savedPosition && to.meta.keepAlive) {
    return savedPosition;
  }
  return { left: 0, top: 0 };
};
const router = createRouter({
  history: createWebHistory("/"),
  routes: routes,
  scrollBehavior,
});

const whiteUrl = [
  "/login",
  "/home",
  "/register",
  "/404",
  "/403",
  "/500",
  "/401",
  "/learn",
];
// 模糊白名单
const whiteUrlFuzzy = ["/learn", "/textbook", "/classify"];
const wordViewTimesUrl = ["/learn"];
router.beforeEach((to: any, from: any, next: NavigationGuardNext) => {
  appStore.handleRouter(to, from);
  const token = useUserStore().token;
  if (
    wordViewTimesUrl.some((item) => to.path.includes(item)) &&
    !useVipStore().check()
  ) {
    const toPath = to.path;
    const isWordDetailPage = toPath.includes("/learn");
    if (isWordDetailPage) {
      return next(() => {
        router.go(-2); // Go back two levels
      });
    } else {
      return next(() => {
        router.go(-1); // Go back one level
      });
    }
  }
  if (token) {
    if (to.path === "/login" || to.path === '/bindPhone' || to.path === '/register') {
      return next({ path: "/" }); // 已登录用户访问登录页时重定向到主页
    } else {
      return next(); // 已登录用户允许正常导航
    }
  } else {
    if (
      whiteUrl.includes(to.path) ||
      whiteUrlFuzzy.some((item) => to.path.includes(item))
    ) {
      return next(); // 未登录用户访问白名单中的页面时允许导航
    } else {
      return next({ path: "/login" }); // 未登录用户重定向到登录页
    }
  }
});

export default router;
